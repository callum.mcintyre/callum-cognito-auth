Based on https://medium.com/@gmonne/custom-authentication-using-aws-cognito-e0b489badc3f


# Notes

- We will need to configure SES emails in the user pool - limit is 50 per day for Cognito only (ie 50 signup confirms etc per day I think?)

- https://github.com/amazon-archives/amazon-cognito-identity-js is the old JS library used for this tutorial, I'll use it for simplicity to keep using the tutorial but we'll need to move to https://github.com/aws-amplify/amplify-js/tree/master/packages/amazon-cognito-identity-js